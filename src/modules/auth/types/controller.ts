export interface IRawLogin {
  email: string;
  password: string;
}
export type IRawRefreshToken = {
  refreshToken: string;
};
export type IRequestData<T> = {
  body: T;
};
export type IResToken = {
  access: {
    token: string;
    expires: string;
  };
  refresh: {
    token: string;
    expires: string;
  };
};
export type IResRegister = {
  user: {
    name: string;
    email: string;
    role: string;
    isEmailVerified: boolean;
  };
  token: IResToken;
};
export type IRawRole = { name: string; aliasName: string };
export type IRawBodyRole = { aliasName: string };
export type IRawParamsRole = { roleName: string };

export type IPermissionRole = {
  canAdd: boolean;
  canDelete: boolean;
  canSoftDelete: boolean;
  canUpdate: boolean;
  canView: boolean;
  canList: boolean;
  moduleName: string;
};
export type IResPermissionList = {
  role: string;
  permissionRole: IPermissionRole[];
};
