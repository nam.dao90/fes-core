import addMinutes from 'date-fns/addMinutes';
import addDays from 'date-fns/addDays';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import configEnv from '@src/config/env';
import Token from '@src/modules/auth/model';
import { IErrorField, IErrorResponse } from '@utils/types';
import { badRequest, handleErrorsModel } from '@utils/apiErrorInput';
import usersServices from '@modules/user/services';
import { ITokenTypes } from './types/model';
import { UserInstance } from '../user/types/model';
import { IResToken } from './types/controller';

const generateToken = (
  userId: string,
  currentDate: Date,
  expires: Date,
  type: ITokenTypes,
  secret = configEnv.jwt.secret
) => {
  const payload = {
    sub: userId,
    iat: currentDate.getTime(),
    exp: expires.getTime(),
    type,
  };
  return jwt.sign(payload, secret);
};

const saveToken = async (
  token: string,
  refreshToken: string,
  userId: string,
  expires: Date,
  type: ITokenTypes,
  blacklisted = false
) => {
  const tokenDoc = await Token.create({
    token,
    refreshToken,
    userId,
    expires,
    type,
    blacklisted,
  });
  return tokenDoc;
};

const generateAuthTokens = async (
  user: UserInstance
): Promise<IResToken> => {
  const currentDate = new Date();
  const accessTokenExpires = addMinutes(
    currentDate,
    configEnv.jwt.accessExpirationMinutes
  );
  const accessToken = generateToken(
    user.uid,
    currentDate,
    accessTokenExpires,
    'access'
  );

  const refreshTokenExpires = addDays(
    currentDate,
    configEnv.jwt.refreshExpirationDays
  );
  const refreshToken = generateToken(
    user.uid,
    currentDate,
    refreshTokenExpires,
    'refresh'
  );
  await saveToken(
    accessToken,
    refreshToken,
    user.uid,
    refreshTokenExpires,
    'refresh'
  );

  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toString(),
    },
    refresh: {
      token: refreshToken,
      expires: refreshTokenExpires.toString(),
    },
  };
};

const verifyToken = async (
  refreshToken: string,
  type: ITokenTypes
) => {
  const payload = jwt.verify(refreshToken, configEnv.jwt.secret);
  const tokenDoc = await Token.findOne({
    where: {
      type,
      userId: payload.sub.toString(),
      blacklisted: false,
      refreshToken,
      deletedAt: null,
    },
  });
  return tokenDoc;
};

const checkExistToken = async (token: string) =>
  Token.findOne({
    where: {
      token,
      blacklisted: false,
    },
  });

const refreshAuth = async (
  refreshToken: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: IResToken | null;
}> => {
  const dataToken = await verifyToken(refreshToken, 'refresh');
  const dataUser = await dataToken?.getUser();
  if (!dataToken || !dataUser) {
    const errorResponse = badRequest('token not found');
    return { error: errorResponse, data: null };
  }
  dataToken.destroy();
  return {
    error: null,
    data: await generateAuthTokens(dataUser),
  };
};
const loginUserWithEmailAndPassword = async (
  email: string,
  password: string
): Promise<{
  error: IErrorResponse<IErrorField[]> | null;
  data: UserInstance | null;
}> => {
  const user = await usersServices.getUserViaEmail(email);
  if (!user || !(await bcrypt.compare(password, user.password))) {
    const errorResponse = handleErrorsModel({
      key: 'email',
      message: 'Incorrect email or password',
    });
    return { error: errorResponse, data: null };
  }
  return { error: null, data: user };
};
const logout = async (
  token: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: string | null;
}> => {
  const tokenUser = await Token.findOne({
    where: {
      token,
      type: 'refresh',
      blacklisted: false,
    },
  });
  if (!tokenUser) {
    const errorResponse = badRequest('token not found');
    return { error: errorResponse, data: null };
  }
  tokenUser.destroy();
  return { error: null, data: 'logout success' };
};
const authServices = {
  generateAuthTokens,
  verifyToken,
  refreshAuth,
  loginUserWithEmailAndPassword,
  logout,
  checkExistToken,
};
export default authServices;
