import { Sequelize } from 'sequelize';
import configEnv from '@src/config/env';
import { sequelizeLogger } from '@src/config/logger';

const {
  postgres: { DB_NAME, DB_PASSWORD, DB_USER, DB_HOSTNAME },
} = configEnv;
const db = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
  host: DB_HOSTNAME,
  dialect: 'postgres',
  pool: {
    max: 20,
    min: 0,
    acquire: 30000,
    idle: 5000,
  },
  dialectOptions: {
    useUTC: false, // for reading from database
  },
  logQueryParameters: true,
  logging: (msg: string) => sequelizeLogger.info(msg),
});

export default db;
