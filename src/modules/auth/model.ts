import {
  TokenCreationAttributes,
  TokenInstance,
  ETokenTypes,
} from '@modules/auth/types/model';
import Sequelize from 'sequelize';
import db from '@src/config/database';
import UserModel from '@modules/user/model';

const TokenModel = db.define<TokenInstance, TokenCreationAttributes>(
  'token',
  {
    userId: {
      type: Sequelize.STRING,
      references: {
        model: UserModel,
        key: 'userId',
      },
    },
    token: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    refreshToken: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    type: {
      type: Sequelize.ENUM(
        ETokenTypes.REFRESH,
        ETokenTypes.RESET_PASSWORD,
        ETokenTypes.VERIFY_EMAIL
      ),
      allowNull: false,
    },
    expires: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    blacklisted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    paranoid: true,
    timestamps: true,
  }
);
UserModel.hasMany(TokenModel, {
  foreignKey: 'userId',
  sourceKey: 'uid', // Important when you want to query with custom column in Tokens table
});
TokenModel.belongsTo(UserModel, {
  foreignKey: 'userId',
  targetKey: 'uid', // Important when you want to query with custom column in Users table
});
export default TokenModel;
