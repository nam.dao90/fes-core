import Sequelize from 'sequelize';
import db from '@config/database';
import { RoleInstance, RoleAttribute } from './types/model';

const RoleModel = db.define<RoleInstance, RoleAttribute>(
  'roles',
  {
    role: {
      allowNull: false,
      type: Sequelize.STRING,
      unique: true,
    },
    aliasRole: {
      type: Sequelize.STRING,
    },
  },
  {
    paranoid: true,
    timestamps: true,
  }
);

export default RoleModel;
