import { Strategy, ExtractJwt, StrategyOptions } from 'passport-jwt';
import { VerifyCallback } from 'jsonwebtoken';
import User from '@modules/user/model';
import { ETokenTypes } from '@modules/auth/types/model';
import RoleModel from '@src/modules/roles/model';
import configEnv from './env';

const jwtOptions: StrategyOptions = {
  secretOrKey: configEnv.jwt.secret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtVerify = async (payload: any, done: VerifyCallback) => {
  try {
    if (payload.type !== ETokenTypes.ACCESS) {
      throw new Error('Invalid token type');
    }
    const user = await User.findOne({
      where: { uid: payload.sub },
      include: [RoleModel],
    });
    if (!user) {
      return done(null, null);
    }
    done(null, user);
  } catch (error) {
    done(error, null);
  }
};

const jwtStrategy = new Strategy(jwtOptions, jwtVerify);

export default jwtStrategy;
