import { Response } from 'express';
import configEnv from '@src/config/env';
import { internalServer } from '@utils/apiErrorInput';
import httpStatus from 'http-status';

export const internalException = (res: Response, err: Error) => {
  if (configEnv.env === 'production') {
    res
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .send(internalServer());
  }
  return res
    .status(httpStatus.INTERNAL_SERVER_ERROR)
    .json({ message: err.message });
};
