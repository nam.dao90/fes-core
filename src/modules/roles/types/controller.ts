export type IRawRole = { name: string; aliasName: string };
export type IRawBodyRole = { aliasName: string };
export type IRawParamsRole = { roleName: string };
