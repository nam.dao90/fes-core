module.exports = {
  apps: [
    {
      name: 'app-test',
      script: 'src/index.ts',
      instances: 1,
      autorestart: false,
      node_args: '-r tsconfig-paths/register -r ts-node/register',
      watch: true,
      time: true,
      env_development: {
        NODE_ENV: 'development',
        PORT: 3000,
      },
      env_test: {
        NODE_ENV: 'test',
        PORT: 3001,
      },
    },
    {
      name: 'app-production',
      script: 'src/index.ts',
      instances: 1,
      autorestart: true,
      node_args: '-r tsconfig-paths/register -r ts-node/register',
      watch: false,
      time: true,
      env_production: {
        NODE_ENV: 'production',
        PORT: 3002,
      },
    },
  ],
  deploy: {
    production: {
      user: 'SSH_USERNAME',
      host: 'SSH_HOSTMACHINE',
      ref: 'origin/master',
      repo: 'GIT_REPOSITORY',
      path: 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy':
        'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': '',
    },
  },
};
