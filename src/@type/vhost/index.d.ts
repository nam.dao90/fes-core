/**
 * Example declare typescript with 3rd is not define
 */
declare module 'vhost' {
  import { RequestHandler } from 'express';

  function vhost(url: string, app: any): RequestHandler;
  export = vhost;
}
