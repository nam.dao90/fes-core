import express from 'express';
import authMiddleWare from '@middleware/auth';
import roles from '@middleware/roles';
import userController from './controller';

const router = express.Router();
router.use(authMiddleWare.authToken);
router
  .route('/')
  .get(
    authMiddleWare.authJwt,
    roles.accessRoleByModule('canList', 'users'),
    userController.getUsers
  )
  .put(
    authMiddleWare.authJwt,
    roles.accessRoleByModule('canUpdate', 'users'),
    userController.updateUser
  )
  .post(
    authMiddleWare.authJwt,
    roles.accessRoleByModule('canAdd', 'users'),
    (req, res) => {
      res.send('create');
    }
  )
  .delete(
    authMiddleWare.authJwt,
    roles.accessRoleByModule('canSoftDelete', 'users'),
    (_req, _res) => {
      _res.send('delete');
    }
  );
export default router;
