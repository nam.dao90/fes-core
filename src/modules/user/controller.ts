import { internalException } from '@utils/exception';
import { Request, Response } from 'express';
import usersResponse from './response';
import usersServices from './services';

const getUsers = async (_req: Request, res: Response) => {
  try {
    const result = await usersServices.queryUsers();
    const resUser = usersResponse.parseUserList(result);
    res.json(resUser);
  } catch (err) {
    internalException(res, err);
  }
};
const updateUser = async (req: Request, res: Response) => {
  try {
    res.json(true);
  } catch (err) {
    internalException(res, err);
  }
};

const userController = { getUsers, updateUser };

export default userController;
