/* eslint-disable @typescript-eslint/no-var-requires */
const bcrypt = require('bcrypt');
const nanoid = require('nanoid');

module.exports = {
  async up(queryInterface) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('roles', [
      {
        role: 'sa',
        aliasRole: 'super admin',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        role: 'member',
        aliasRole: 'member',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
    const salt = bcrypt.genSaltSync(10);
    const passDefault = await bcrypt.hash('password1', salt);
    await queryInterface.bulkInsert('users', [
      {
        idRole: 1,
        uid: nanoid.nanoid(),
        name: 'superadmin',
        email: 'sa@gmail.com',
        password: passDefault,
        isEmailVerified: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('roles', null, {});
    await queryInterface.bulkDelete('users', null, {});
  },
};
