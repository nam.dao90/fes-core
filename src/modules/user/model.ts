import validator from 'validator';
import bcrypt from 'bcrypt';
import {
  UserCreationAttributes,
  UserInstance,
} from '@modules/user/types/model';
import db from '@src/config/database';
import Sequelize from 'sequelize';
import RoleModel from '../roles/model';

const UserModel = db.define<UserInstance, UserCreationAttributes>(
  'users',
  {
    uid: {
      allowNull: false,
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
    },
    name: {
      type: Sequelize.STRING(50),
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING(50),
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          msg: 'Email is not valid',
        },
        notEmpty: {
          msg: 'Email must be not empty',
        },
        isLowercase: {
          msg: 'Email must be lowercase',
        },
        validateEmail(value: string) {
          if (!validator.isEmail(value)) {
            throw new Error('Invalid email');
          }
        },
      },
    },
    password: {
      type: Sequelize.STRING(128),
      allowNull: false,
      validate: {
        len: {
          msg: 'password must be at least 6 characters',
          args: [6, 10],
        },
        notNull: {
          msg: 'Password must be not empty',
        },
        notEmpty: {
          msg: 'Password must be not empty',
        },
        rulePassword: (value: string) => {
          if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
            throw new Error(
              'Password must contain at least one letter and one number'
            );
          }
        },
      },
    },
    isEmailVerified: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    timestamps: true,
    paranoid: true,
  }
);
UserModel.beforeSave(async user => {
  const salt = bcrypt.genSaltSync(10);
  user.password = await bcrypt.hash(user.password, salt);
});
RoleModel.hasMany(UserModel, {
  foreignKey: 'idRole',
});
UserModel.belongsTo(RoleModel, {
  foreignKey: 'idRole',
  targetKey: 'id',
});
export default UserModel;
