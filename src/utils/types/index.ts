export type IStatus = {
  code: number;
  success: boolean;
};
export type IErrorField = {
  key: string;
  message: string;
};

export type IErrorResponse<T> = {
  status: IStatus;
  data: T;
};

export type IClientResponse<T> = {
  status: IStatus;
  data: T;
};

export type IRequestData<T> = {
  body: T;
};
