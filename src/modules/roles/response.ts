import { resStatusSuccess } from '@utils/status';
import { IClientResponse } from '@utils/types';
import { RoleAttribute } from './types/model';

const parseRoles = (
  roleAttr: RoleAttribute
): IClientResponse<{ role: string; alias: string }> => {
  return {
    status: resStatusSuccess(),
    data: { role: roleAttr.role, alias: roleAttr.aliasRole },
  };
};

const parseListRoles = (
  lsDocumentRoles: RoleAttribute[]
): IClientResponse<Array<{ alias: string; role: string }>> => {
  const parsRole = lsDocumentRoles.map(r => {
    return {
      alias: r.aliasRole,
      role: r.role,
    };
  });
  return {
    status: resStatusSuccess(),
    data: parsRole,
  };
};

const rolesResponse = {
  parseRoles,
  parseListRoles,
};
export default rolesResponse;
