import { ValidationError } from 'joi';
import httpStatus from 'http-status';
import configEnv from '@src/config/env';
import ApiError from '@utils/apiError';
import { logger } from '@config/logger';
import { IErrorField, IErrorResponse } from './types';
import {
  resStatusAccessDenied,
  resStatusBadRequest,
  resStatusInputFailure,
  resStatusInternalSever,
  resStatusNotFound,
  resStatusUnAuthorize,
} from './status';

const showErrorLogRequest = (error: ValidationError) => {
  const errorMessage = error.details
    .map(details => details.message)
    .join(', ');
  const dataError = new ApiError(
    httpStatus.BAD_REQUEST,
    errorMessage
  );
  logger.error(dataError);
};
export const handleErrorsRequest = (
  error: ValidationError
): IErrorResponse<Array<IErrorField>> => {
  if (configEnv.env === 'production') {
    showErrorLogRequest(error);
  }
  const lsErrorField: Array<IErrorField> = [];
  if (error.details.length > 0) {
    error.details.forEach(err => {
      const { key = '' } = err.context;
      const { message } = err;
      lsErrorField.push({
        key,
        message: message.replace(/"/g, ''),
      });
    });
  }
  return {
    status: resStatusInputFailure(),
    data: lsErrorField,
  };
};

export const handleErrorsModel = (
  error: IErrorField
): IErrorResponse<Array<IErrorField>> => {
  if (configEnv.env === 'production') {
    const dataError = new ApiError(
      httpStatus.UNPROCESSABLE_ENTITY,
      error.message
    );
    logger.error(dataError);
  }
  return {
    status: resStatusInputFailure(),
    data: [error],
  };
};

export const internalServer = () => {
  return {
    status: resStatusInternalSever(),
    data: {
      message: httpStatus[500],
    },
  };
};

export const unauthorizeUser = () => {
  return {
    status: resStatusUnAuthorize(),
    data: {
      message: httpStatus[401],
    },
  };
};

export const accessDeniedUser = () => {
  return {
    status: resStatusAccessDenied(),
    data: {
      message: httpStatus[403],
    },
  };
};

export const badRequest = (errorMessage: string) => {
  return {
    status: resStatusBadRequest(),
    data: {
      message: errorMessage || httpStatus.NOT_FOUND,
    },
  };
};

export const notfound = () => {
  return {
    status: resStatusNotFound(),
    data: {
      message: httpStatus[404],
    },
  };
};
